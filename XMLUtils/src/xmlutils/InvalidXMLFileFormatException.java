package xmlutils;

/**
 * Created by SewardC on 2016/11/12.
 */
public class InvalidXMLFileFormatException extends Exception{

    // NAME OF XML FILE THAT DID NOT VALIDATE
    private String xmlFileWithError;

    // NAME OF XML SCHEMA USED FOR VALIDATION
    private String xsdFile;

    /**
     * Constructor for this exception, these are simple objects, we'll just store some
     * info about the error
     *
     * @param initXMLFileWithError XML doc file name that didn't validate
     * @param initXSDFile   XML schema file used in validation
     */

    public InvalidXMLFileFormatException(String initXMLFileWithError, String initXSDFile){
        // KEEP IT FOR LATER
        xmlFileWithError = initXMLFileWithError;
        xsdFile = initXSDFile;
    }

    public InvalidXMLFileFormatException(String initXMLFileWithError){
        xmlFileWithError = initXMLFileWithError;
    }
    public String toString(){
        return "XML Document (" + xmlFileWithError
                + ") does not conform to Schema (" + xsdFile + ")";
    }
}
