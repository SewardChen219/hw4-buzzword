package settings;

/**
 * Created by SewardC on 2016/11/12.
 */
public enum InitializationParameters {

    APP_PROPERTIES_XML("app-properties.xml"),
    WORKSPACE_PROPERTIES_XML("workspace-properties.xml"),
    PROPERTIES_SCHEMA_XSD("properties-schema.xsd"),
    ERROR_EXIT_DIALOG_BUTTON_LABEL("Exit application."),
    SUCCESS_DIALOG_BUTTON_LABEL("Program loaded sucessfully!"),
    APP_WORKDIR_PATH("saved"),
    APP_TABLE_WORKDIR_PATH("/tableOfContent.json"),
    APP_IMAGEDIR_PATH("images"),
    APP_WORDDIR_PATH("C:/Users/SewardC/Desktop/College/CSE219/BuzzWordGame/BuzzWord/resources/words");

    private String parameter;

    InitializationParameters(String parameter){
        this.parameter = parameter;
    }

    public String getParameter(){return parameter;}
}
