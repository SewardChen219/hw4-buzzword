package ui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import settings.InitializationParameters;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

//import static settings.InitializationParameters.ERROR_DIALOG_BUTTON_LABEL;

/**
 * This class serves to present custom text messages to the user when
 * events occur. Note that it always provides the same controls, a label
 * with a message, and a single ok button.
 *
 * @author Richard McKenna, Ritwik Banerjee
 * @author ?
 * @version 1.0
 */
public class AppMessageDialogSingleton extends Stage implements Initializable{

    static AppMessageDialogSingleton singleton = null;

    @FXML
     BorderPane root;
    Scene  messageScene;

    @FXML
    Label  messageLabel;

    @FXML
    Button closeButton;

    public AppMessageDialogSingleton() { }

    /**
     * A static accessor method for getting the singleton object.
     *
     * @return The one singleton dialog of this object type.
     */
    public static AppMessageDialogSingleton getSingleton() {
        if (singleton == null)
            singleton = new AppMessageDialogSingleton();
        return singleton;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    /**
     * This function fully initializes the singleton dialog for use.
     *
     * @param owner The window above which this dialog will be centered.
     */
    public void init(Stage owner) throws IOException {
        messageLabel = new Label();
        closeButton = new Button();
            root = FXMLLoader.load(
                    new URL(AppMessageDialogSingleton.class.getResource("AppMessageDialogSingletonScene.fxml").
                            toExternalForm()));

        // MAKE IT MODAL AND UNDECORATED
        initStyle(StageStyle.UNDECORATED);
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);


        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(root);
        this.setScene(messageScene);
    }

    public void setOnAction(){
        this.close();
    }

    public void changeCloseButtonText(String str){
        ((Button)root.lookup("#closeButton")).setText(str);
        ((Button)root.lookup("#closeButton")).setOnAction(e -> this.setOnAction());
    }

    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     *
     * @param message Message to appear inside the dialog.
     */
    public void show(String message){
        // SET THE DIALOG TITLE BAR TITLE
        //setTitle(title);
        // SET THE MESSAGE TO DISPLAY TO THE USER

        ((Label) root.lookup("#messageLabel")).setText(message);

        // AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
        // WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
        // DO MORE WORK.
        showAndWait();
    }
}