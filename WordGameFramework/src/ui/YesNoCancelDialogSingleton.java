package ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;

/**
 * Created by SewardC on 2016/11/12.
 */
public class YesNoCancelDialogSingleton extends Stage{
    // HERE'S THE SINGLETON
    static YesNoCancelDialogSingleton singleton;

    // GUI CONTROLS FOR OUR DIALOG

    @FXML
    BorderPane root;
    Scene messageScene;

    @FXML
    public Label messageLabel;

    @FXML
    public Button yesButton;

    @FXML
    public Button noButton;

    @FXML
    public Button cancelButton;

    String selection;

    // CONSTANT CHOICES
    public static final String YES    = "Yes";
    public static final String NO     = "No";
    public static final String CANCEL = "Cancel";

    /**
     * Note that the constructor is private since it follows
     * the singleton design pattern.
     */
    public YesNoCancelDialogSingleton() {}

    /**
     * The static accessor method for this singleton.
     *
     * @return The singleton object for this type.
     */
    public static YesNoCancelDialogSingleton getSingleton() throws IOException{
        if (singleton == null)
            singleton = new YesNoCancelDialogSingleton();
        return singleton;
    }

    /**
     * This method initializes the singleton for use.
     *
     * @param primaryStage The window above which this dialog will be centered.
     */
    public void init(Stage primaryStage) throws IOException {
        messageLabel = new Label();
        root = FXMLLoader.load(
                new URL(AppMessageDialogSingleton.class.getResource("YesNoCancelDialogSingleton.fxml").
                        toExternalForm()));

        ((Button)root.lookup("#yesButton")).setText(YES);
        ((Button)root.lookup("#noButton")).setText(NO);
        ((Button)root.lookup("#cancelButton")).setText(CANCEL);

        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initStyle(StageStyle.UNDECORATED);
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);


        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(root);
        this.setScene(messageScene);
    }

    public void setOnAction(ActionEvent e){
            if(e.getSource() == yesButton)
                System.exit(0);
            else
                this.close();
    }
    /**
     * Accessor method for getting the selection the user made.
     *
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }



    /**
     * This method loads a custom message into the label
     * then pops open the dialog.
     *
     * @param message Message to appear inside the dialog.
     */
    public void show(String message) {
        // SET THE DIALOG TITLE BAR TITLE

        // SET THE MESSAGE TO DISPLAY TO THE USER
        ((Label) root.lookup("#messageLabel")).setText(message);

        // AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
        // WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
        // DO MORE WORK.
        showAndWait();
    }

}
