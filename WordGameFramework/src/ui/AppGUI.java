package ui;

import apptemplate.AppTemplate;
import components.AppStyleArbiter;
import controller.*;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import propertymanager.PropertyManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;

/**
 * Created by SewardC on 2016/11/12.
 */
public class AppGUI implements AppStyleArbiter{

    protected FileController    fileController; // to react to file-related controls
    protected GamePlayController gamePlayController; // to react to game-play-related controls
    protected LevelChoiceController lcController;   // to react to level-choice-related controls
    protected ModeChoiceController  mcController;   // to react to mode-choice-related controls
    protected ProfileSettingController pfController; //to react to profile-setting-related controls

    protected Stage             primaryStage;
    protected Scene             primaryScene;
    protected BorderPane        appPane;        // the root node in the scene graph, to organize the containers
    protected String            applicationTitle;    // the application title
    protected FlowPane          toolbarPane;      // the top toolbar
    protected Button            newButton;        // button to create a new instance of the application
    protected Button            exitButton;

    private int appSpecificWindowWidth;  // optional parameter for window width that can be set by the application
    private int appSpecificWindowHeight; // optional parameter for window height that can be set by the application

    public AppGUI(Stage initPrimaryStage, String initAppTitle, AppTemplate app) throws IOException,
            InstantiationException {
        this(initPrimaryStage, initAppTitle, app, -1, -1);
    }

    public AppGUI(Stage primaryStage, String applicationTitle, AppTemplate appTemplate,
                  int appSpecificWindowWidth, int appSpecificWindowHeight) throws IOException, InstantiationException {
        this.appSpecificWindowWidth = appSpecificWindowWidth;
        this.appSpecificWindowHeight = appSpecificWindowHeight;
        this.primaryStage = primaryStage;
        this.applicationTitle = applicationTitle;
        initializeToolbar();
        initializeToolbarHandlers(appTemplate);
        initializeWindow();                     // start the app window (without the application-specific workspace)
        primaryStage.setOnCloseRequest(e -> {});
    }

    private void initializeToolbar() throws IOException {
        toolbarPane = new FlowPane();
        newButton = initializeChildButton(toolbarPane, NEW_ICON.toString(), NEW_TOOLTIP.toString(), false);
        exitButton = initializeChildButton(toolbarPane, EXIT_ICON.toString(), EXIT_TOOLTIP.toString(), false);
    }

    private void initializeToolbarHandlers(AppTemplate app) throws InstantiationException, IOException {
        PropertyManager propertyManager             = PropertyManager.getManager();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        try {
            Method getFileControllerClassMethod = app.getClass().getMethod("getFileControllerClass");
            String         fileControllerClassName      = (String) getFileControllerClassMethod.invoke(app);
            Class<?>       klass                        = Class.forName("controller." + fileControllerClassName);
            Constructor<?> constructor                  = klass.getConstructor(AppTemplate.class);

            fileController = (FileController) constructor.newInstance(app);
            app.setFileController(fileController);

        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | ClassNotFoundException e) {
            System.exit(0);
            dialog.show(propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
        }
        newButton.setOnAction(e -> fileController.handleNewRequest());
        exitButton.setOnAction(e -> fileController.handleExitRequest());
    }

    public Button initializeChildButton(Pane toolbarPane, String icon, String tooltip, boolean disabled) throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();

        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resources folder does not exist.");

        Button button = new Button();
        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(icon)))) {
            Image buttonImage = new Image(imgInputStream);
            button.setDisable(disabled);
            button.setGraphic(new ImageView(buttonImage));
            Tooltip buttonTooltip = new Tooltip(propertyManager.getPropertyValue(tooltip));
            button.setTooltip(buttonTooltip);
            toolbarPane.getChildren().add(button);
        } catch (URISyntaxException e) {
            dialog.show(propertyManager.getPropertyValue(URL_SYNTAX_ERROR_MESSAGE));
            System.exit(1);
        }

        return button;
    }

    public Scene getPrimaryScene(){
        return primaryScene;
    }

    public FlowPane getToolbarPane() { return toolbarPane; }

    public BorderPane getAppPane(){ return appPane;}

    public Stage getWindow(){
        return primaryStage;
    }

    private void initializeWindow() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();

        // SET THE WINDOW TITLE
        primaryStage.setTitle(applicationTitle);

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        appPane = new BorderPane();

        /**
        char[] urlChars = AppGUI.class.getResource("").toExternalForm().toCharArray();
        int numOfSlashes = 0;
        for(int i = 0; i < urlChars.length; i++){
            if(urlChars[i] == '/') numOfSlashes ++;
        }

        System.out.println(AppGUI.class.getResource("").toExternalForm());
        int neededSlashes = numOfSlashes - 2;
        numOfSlashes = 0;
        String neededURL = "";
        for(int i = 0; i < urlChars.length; i++){
            if(numOfSlashes != neededSlashes)
                neededURL += urlChars[i];
            if(urlChars[i] == '/' && numOfSlashes != neededSlashes) numOfSlashes++;
        }

        neededURL += "BuzzWord/controller/Homescreen.fxml";

        System.out.println(neededURL);

        appPane = FXMLLoader.load(new URL(neededURL));

         */

        appPane.setTop(toolbarPane);
        primaryScene = appSpecificWindowWidth < 1 || appSpecificWindowHeight < 1 ? new Scene(appPane)
                : new Scene(appPane,
                appSpecificWindowWidth,
                appSpecificWindowHeight);

        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resrouces folder does not exist.");
        try (InputStream appLogoStream = Files.newInputStream(Paths.get(imgDirURL.toURI())
                .resolve(propertyManager.getPropertyValue(APP_LOGO)))) {
            primaryStage.getIcons().add(new Image(appLogoStream));
        } catch (URISyntaxException e) {
            dialog.show( propertyManager.getPropertyValue(URL_SYNTAX_ERROR_MESSAGE));
        }

        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    @Override
    public void initStyle() {
        // currently, we do not provide any stylization at the framework-level
    }
}
