package components;

import javafx.scene.layout.Pane;

/**
 * Created by SewardC on 2016/11/12.
 */
public abstract class AppWorkspaceComponent implements AppStyleArbiter{

    protected Pane workspace;

    public void setWorkspace(Pane initWorkspace){workspace = initWorkspace;}

    public Pane getWorkspace(){return workspace;}

    public abstract void reloadWorkspace();
}
