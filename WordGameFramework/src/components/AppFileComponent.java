package components;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Created by SewardC on 2016/11/12.
 */
public interface AppFileComponent {

    void saveData(AppDataComponent data, Path filePath) throws IOException;

    void loadData(AppDataComponent data, Path filePath) throws IOException;

    void exportData(AppDataComponent data, Path filePath) throws IOException;

    boolean checkData(String dataStr, Path filePath) throws IOException;

    boolean checkData2(String name, String password, Path filePath) throws IOException;

    void initTable(Path filePath) throws IOException;
}
