package components;

/**
 * Created by SewardC on 2016/11/12.
 */
public interface AppStyleArbiter {

    String CLASS_BORDERED_PANE = "bordered_pane";

    void initStyle();
}
