package components;

/**
 * Created by SewardC on 2016/11/12.
 */
public interface AppComponentBuilder {

    AppDataComponent buildDataComponent() throws Exception;

    AppFileComponent buildFileComponent() throws Exception;

    AppWorkspaceComponent buildWorkspaceComponent() throws Exception;
}
