package components;

/**
 * Created by SewardC on 2016/11/12.
 */
public interface AppDataComponent {

    void reset();
}
