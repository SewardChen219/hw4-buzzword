package apptemplate;

import components.AppComponentBuilder;
import components.AppDataComponent;
import components.AppFileComponent;
import components.AppWorkspaceComponent;
import controller.*;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import propertymanager.PropertyManager;
import settings.InitializationParameters;
import ui.AppGUI;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;
import xmlutils.InvalidXMLFileFormatException;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;

import static settings.AppPropertyType.*;
import static settings.AppPropertyType.APP_PATH_CSS;
import static settings.InitializationParameters.*;

/**
 * Created by SewardC on 2016/11/12.
 */
public abstract class AppTemplate extends Application {

    public final PropertyManager    propertyManager = PropertyManager.getManager();
    private AppDataComponent        dataComponent;  // to manage the app's data
    private AppFileComponent        fileComponent;  // to manage the app's file I/O
    private FileController          fileController;
    private GamePlayController      gamePlayController;
    private LevelChoiceController   levelChoiceController;
    private ModeChoiceController    modeChoiceController;
    private ProfileSettingController    psController;
    private EditingController       editingController;

    private AppWorkspaceComponent   workspaceComponent;
    private AppGUI                  gui;

    public EditingController getEditController() {return editingController;}

    public void setEditingController(EditingController editingController){this.editingController = editingController;}

    public FileController getFileController() {return fileController;}

    public void setFileController(FileController fileController) {
        this.fileController = fileController;
    }

    public GamePlayController getGamePlayController(){return gamePlayController;}

    public void setGamePlayController(GamePlayController gamePlayController){
        this.gamePlayController = gamePlayController;
    }

    public LevelChoiceController getLevelChoiceController(){return levelChoiceController;}

    public void setLevelChoiceController(LevelChoiceController levelChoiceController){
        this.levelChoiceController = levelChoiceController;
    }

    public ModeChoiceController getModeChioceController(){return modeChoiceController;}

    public void setModeChoiceController(ModeChoiceController modeChoiceController){
        this.modeChoiceController = modeChoiceController;
    }

    public ProfileSettingController getProfileSettingController(){return psController;}

    public void setProfileSettingController(ProfileSettingController psController){
        this.psController = psController;
    }

    public abstract AppComponentBuilder makeAppBuilderHook();

    public AppDataComponent getDataComponent(){return dataComponent;}

    public void setDataComponent(AppDataComponent dataComponent){
        this.dataComponent = dataComponent;
    }

    public AppFileComponent getFileComponent(){return fileComponent;}

    public void setFileComponnet(AppFileComponent fileComponent){
        this.fileComponent = fileComponent;
    }

    public AppWorkspaceComponent getWorkspaceComponent(){return workspaceComponent;}

    public AppGUI   getGUI() {return gui;}

    public String getFileControllerClass(){ return ""; }

    public void start(Stage primaryStage) throws IOException{
        AppMessageDialogSingleton  messageDialog = AppMessageDialogSingleton.getSingleton();
        YesNoCancelDialogSingleton yesNoDialog   = YesNoCancelDialogSingleton.getSingleton();
        messageDialog.init(primaryStage);
        yesNoDialog.init(primaryStage);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        System.out.println("1");
        try{
            if(loadProperties(APP_PROPERTIES_XML) && loadProperties(WORKSPACE_PROPERTIES_XML)){
                AppComponentBuilder builder = makeAppBuilderHook();
                fileComponent = builder.buildFileComponent();
                dataComponent = builder.buildDataComponent();
                gui = (propertyManager.hasProperty(APP_WINDOW_WIDTH) && propertyManager.hasProperty(APP_WINDOW_HEIGHT))
                        ? new AppGUI(primaryStage, propertyManager.getPropertyValue(APP_TITLE.toString()), this,
                            Integer.parseInt(propertyManager.getPropertyValue(APP_WINDOW_WIDTH)),
                            Integer.parseInt(propertyManager.getPropertyValue(APP_WINDOW_HEIGHT)))
                        : new AppGUI(primaryStage, propertyManager.getPropertyValue(APP_TITLE.toString()), this);
                System.out.println("2");
                workspaceComponent = builder.buildWorkspaceComponent();
                primaryStage.setMaxWidth(700);
                primaryStage.setMaxHeight(450);
                primaryStage.setX(320);
                primaryStage.setY(150);
                initStyleSheet();

                gui.initStyle();
                workspaceComponent.initStyle();
            }
        }catch(Exception e){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(PROPERTIES_LOAD_ERROR_MESSAGE.toString()));
        }
    }

    public boolean loadProperties(InitializationParameters propertyParameter) throws IOException{
        try{
            propertyManager.loadProperties(AppTemplate.class, propertyParameter.getParameter(),
                    PROPERTIES_SCHEMA_XSD.getParameter());
        }catch(InvalidXMLFileFormatException e){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(PROPERTIES_LOAD_ERROR_MESSAGE.toString()));
            return false;
        }

        return true;
    }
    public void initStyleSheet() {

        URL cssResource = getClass().getClassLoader().getResource(propertyManager.getPropertyValue(APP_PATH_CSS) +
                File.separator +
                propertyManager.getPropertyValue(APP_CSS));
        assert cssResource != null;
        gui.getPrimaryScene().getStylesheets().add(cssResource.toExternalForm());

    }
}
