package controller;

/**
 * Created by SewardC on 2016/11/13.
 */
public interface ModeChoiceController {
    void handleModeChoiceRequest();
}
