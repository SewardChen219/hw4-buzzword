package controller;

/**
 * Created by SewardC on 2016/11/12.
 */
public interface LevelChoiceController {

    void handleLevelSelectionRequest();

    void handleReturnRequest();
}
