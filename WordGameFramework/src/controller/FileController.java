package controller;

import java.io.IOException;

/**
 * Created by SewardC on 2016/11/12.
 */
public interface FileController {

    void handleNewRequest();

    void handleCreateNewProfileRequest();

    void handleLoginOrLogoutRequest() throws IOException;

    void handleShowProfileSettingRequest();

    void handleSaveRequest() throws IOException;

    void handleHelpRequest() throws IOException;

    void handleExitRequest();


}
