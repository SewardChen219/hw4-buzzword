package controller;

import java.io.IOException;

/**
 * Created by SewardC on 2016/12/11.
 */
public interface EditingController {
    void handleCompleteAction() throws IOException;
}
