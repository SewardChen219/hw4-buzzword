package controller;

/**
 * Created by SewardC on 2016/11/12.
 */
public interface GamePlayController {

    void start();

    void playWithTyping();

    void playWithMouseDrag();

    void handlePauseRequest();

    void handleResumeRequest();

    void handleReplayRequest();

    void handleNextRequest();
}