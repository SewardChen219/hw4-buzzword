package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.*;
import data.GameData;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.FXML;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import propertymanager.PropertyManager;
import settings.AppPropertyType;
import ui.AppGUI;
import ui.AppMessageDialogSingleton;

import java.io.IOException;
import java.net.URL;

import static buzzword.BuzzWordProperties.*;
import static settings.AppPropertyType.*;

import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by SewardC on 2016/11/12.
 */
public class Workspace extends AppWorkspaceComponent {

    private int second;
    private Timer timer;
    private int totalSecond;
    private DecimalFormat format = new DecimalFormat("00");

    private PropertyManager propertyManager = PropertyManager.getManager();
    private URL location;
    private AppTemplate         appTemplate;
    private AppGUI              gui;
    private Stage               primaryStage;
    private Stage               loginScreen;
    private Stage               pauseScreen;

    private BuzzFileController  fileController;
    private BuzzModeChoiceController   modeChoiceController;
    private BuzzLevelChoiceController levelChoiceController;
    private BuzzGameplayController  gamePlayController;
    private BuzzEditingController editController;

    @FXML
    protected BorderPane        root;
    protected GridPane          root2;

    protected BorderPane         homeScreenPane;
    protected BorderPane         createNewProfilePane;
    protected GridPane           loginScreenPane;
    protected BorderPane         modeChoiceScreenPane;
    protected BorderPane         levelChoiceScreenPane;
    protected BorderPane         gamePlayScreenPane;
    protected BorderPane         pauseScreenPane;
    protected BorderPane         helpPane;
    protected BorderPane         profileSettingPane;

    protected Scene              homeScreenScene;
    protected Scene              createNewProfileScene;
    protected Scene              loginScreenScene;
    protected Scene              modeChoiceScreenScene;
    protected Scene              levelChoiceScreenScene;
    protected Scene              gamePlayScreenScene;
    protected Scene              pauseScreenScene;
    protected Scene              helpScene;
    protected Scene              profileSettingScene;



    public Workspace(AppTemplate appTemplate) throws IOException{
        System.out.println("3");
        this.appTemplate = appTemplate;
        gui = appTemplate.getGUI();
        setUpFXML();
        primaryStage = appTemplate.getGUI().getWindow();
        root = appTemplate.getGUI().getAppPane();

        loginScreen = new Stage();
        loginScreen.initStyle(StageStyle.UNDECORATED);

        loginScreen.initModality(Modality.WINDOW_MODAL);
        loginScreen.initOwner(primaryStage);

        pauseScreen = new Stage();
        pauseScreen.initStyle(StageStyle.UNDECORATED);
        pauseScreen.initModality(Modality.WINDOW_MODAL);
        pauseScreen.initOwner(primaryStage);
    }

    public FXMLLoader makefxmlLoader(AppPropertyType url){
        location = getClass().getResource(propertyManager.getPropertyValue(url));
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        return fxmlLoader;
    }
    public void setUpFXML(){
        try {
            FXMLLoader fxmlLoader = makefxmlLoader(HOME_FXML);
            root = fxmlLoader.load(location.openStream());
            appTemplate.setFileController(fxmlLoader.getController());
            fileController = (BuzzFileController) appTemplate.getFileController();
            fileController.setAppTemplate(appTemplate);
            homeScreenPane = root;
            homeScreenScene = new Scene(homeScreenPane);

            fxmlLoader.setController((BuzzFileController)appTemplate.getFileController());
            root2 = fxmlLoader.load(new URL(Workspace.class.getResource(propertyManager.
                    getPropertyValue(LOGIN_FXML)).toExternalForm()));
            appTemplate.setFileController(fxmlLoader.getController());
            loginScreenPane = root2;
            loginScreenScene = new Scene(loginScreenPane);
            fileController = (BuzzFileController) appTemplate.getFileController();
            fileController.setAppTemplate(appTemplate);

            FXMLLoader fxmlLoader2 = makefxmlLoader(CREATE_FXML);
            root = fxmlLoader2.load(location.openStream());
            appTemplate.setProfileSettingController(fxmlLoader2.getController());
            BuzzProfileSettingController profileSettingController = (BuzzProfileSettingController)
                    appTemplate.getProfileSettingController();
            profileSettingController.setAppTemplate(appTemplate);

            createNewProfilePane = root;
            createNewProfileScene = new Scene(createNewProfilePane);

            FXMLLoader fxmlLoader3 = makefxmlLoader(MODE_FXML);
            root = fxmlLoader3.load(location.openStream());

            appTemplate.setModeChoiceController(fxmlLoader3.getController());
            modeChoiceController = (BuzzModeChoiceController)
                    appTemplate.getModeChioceController();
            modeChoiceController.setAppTemplate(appTemplate);

            modeChoiceScreenPane = root;
            modeChoiceScreenScene = new Scene(modeChoiceScreenPane);

            root = fxmlLoader3.load(new URL(Workspace.class.getResource(propertyManager.
                    getPropertyValue(HELP_FXML)).toExternalForm()));
            helpPane = root;
            helpScene = new Scene(helpPane);


            FXMLLoader fxmlLoader4 = makefxmlLoader(LEVEL_FXML);
            root = fxmlLoader4.load(location.openStream());
            appTemplate.setLevelChoiceController(fxmlLoader4.getController());
            levelChoiceController = (BuzzLevelChoiceController) appTemplate.getLevelChoiceController();
            levelChoiceController.setAppTemplate(appTemplate);

            levelChoiceScreenPane = root;
            levelChoiceScreenScene = new Scene(levelChoiceScreenPane);

            FXMLLoader fxmlLoader5 = makefxmlLoader(GAME_FXML);
            root = fxmlLoader5.load(location.openStream());
            appTemplate.setGamePlayController(fxmlLoader5.getController());
            gamePlayController = (BuzzGameplayController) appTemplate.getGamePlayController();
            gamePlayController.setAppTemplate(appTemplate);
            gamePlayScreenPane = root;
            gamePlayScreenScene = new Scene(gamePlayScreenPane);

            fxmlLoader5.setController((BuzzGameplayController)appTemplate.getGamePlayController());
            root = fxmlLoader5.load(new URL(Workspace.class.getResource(propertyManager.
                    getPropertyValue(GAME_PAUSE_FXML)).toExternalForm()));
            gamePlayController = (BuzzGameplayController) appTemplate.getGamePlayController();
            gamePlayController.setAppTemplate(appTemplate);
            pauseScreenPane = root;
            pauseScreenScene = new Scene(pauseScreenPane);

            System.out.println("4");
            FXMLLoader fxmlLoader6 = makefxmlLoader(PROFILE_FXML);
            System.out.println("5");
            root = fxmlLoader6.load(location.openStream());
            appTemplate.setEditingController(fxmlLoader6.getController());
            System.out.println("6");
            editController = (BuzzEditingController) appTemplate.getEditController();
            System.out.println("7");
            editController.setAppTemplate(appTemplate);
            System.out.println("8");
            profileSettingPane = root;
            profileSettingScene = new Scene(profileSettingPane);


        }catch(IOException e){
            e.printStackTrace();
        }
    }

    // Do we need this?
    public void setUpHandler(){
    /*
        controller = new BuzzWordController(appTemplate,
                (BuzzFileController) appTemplate.getFileController(),
                (BuzzGameplayController) appTemplate.getGamePlayController(),
                (BuzzLevelChoiceController) appTemplate.getLevelChoiceController(),
                (BuzzModeChoiceController) appTemplate.getModeChioceController(),
                (BuzzProfileSettingController) appTemplate.getProfileSettingController());
        controller.getBuzzFileController().getNewButton().setOnMouseClicked(e ->{
            controller.getBuzzFileController().handleCreateNewProfileRequest();
        });
        controller.getBuzzFileController().getLoginButton().setOnMouseClicked(e ->{
            try {
                controller.getBuzzFileController().handleLoginOrLogoutRequest();
            }catch(IOException ioe){}
        });
        controller.getBuzzFileController().getExitButton().setOnMouseClicked(e ->{
            controller.getBuzzFileController().handleExitRequest();
        });
        */
    }

    @Override
    public void reloadWorkspace(){
        // Load data into workspace
    };

    public void initStyle(){

        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().
                add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

    }

    public BorderPane getModeChoiceScreenPane(){return modeChoiceScreenPane;}

    public BorderPane getLevelChoiceScreenPane(){return levelChoiceScreenPane;}

    public BorderPane getHelpPane(){return helpPane;}

    public BorderPane getProfileSettingPane(){return profileSettingPane;}

    public BorderPane getGamePlayScreenPane(){return gamePlayScreenPane;}

    public void reinitialize(int numCase){
        switch(numCase){
            case 0:
                loginScreen.close();
                primaryStage.setScene(homeScreenScene);
                break;
            case 1:
                primaryStage.setScene(createNewProfileScene);
                break;
            case 2:
                loginScreen.setScene(loginScreenScene);
                loginScreen.showAndWait();
                break;
            case 3:
                loginScreen.close();
                ((Button) modeChoiceScreenPane.lookup("#logOutButton")).setText(
                        ((GameData)appTemplate.getDataComponent()).getName());
                primaryStage.setScene(modeChoiceScreenScene);
                break;
            case 4:
                ((Button) levelChoiceScreenPane.lookup("#logOutButton")).setText(
                        ((GameData)appTemplate.getDataComponent()).getName());
                ((Label) levelChoiceScreenPane.lookup("#modeLabel")).setText(modeChoiceController.getMode());

                primaryStage.setScene(levelChoiceScreenScene);
                break;
            case 5:
                pauseScreen.close();
                ((Label) gamePlayScreenPane.lookup("#modeLabel")).setText(modeChoiceController.getMode());
                ((Button) gamePlayScreenPane.lookup("#logOutButton")).setText(
                        ((GameData)appTemplate.getDataComponent()).getName());
                primaryStage.setScene(gamePlayScreenScene);
                levelChoiceController.timeClock();
                break;
            case 6:
                pauseScreen.setScene(pauseScreenScene);
                timer.cancel();
                pauseScreen.showAndWait();
                break;
            case 7:
                primaryStage.setScene(helpScene);
                break;
            case 8:
                primaryStage.setScene(profileSettingScene);
            default:
        }

    }

    public void setTime(int second){
        this.second = second;
    }

    public void setTotalTime(int totalSecond){
        this.totalSecond = totalSecond;
    }

    public int getTime(){
        return second * 1000;
    }

    public void resetTimer(){
        timer = new Timer();
    }

    public Timer getTimer(){
        return timer;
    }


    public TimerTask getTimeTask(){
        return new TimerTask() {
            @Override
            public void run() {
                ReentrantLock lock = new ReentrantLock();
                lock.lock();
                Platform.runLater(new Runnable() {
                public void run () {
                    ((Label) gamePlayScreenPane.lookup("#timerLabel")).setText("Time Remaining: "
                            + format.format(totalSecond) + "s");
                    totalSecond--;
                    if(totalSecond == -1) {
                        if(((GameData)appTemplate.getDataComponent()).getWinOrLose()) {
                            AppMessageDialogSingleton appMessageDialogSingleton = AppMessageDialogSingleton.getSingleton();
                            appMessageDialogSingleton.show(propertyManager.getPropertyValue(LOSE_MESSAGE));

                            // losing state
                        }
                        timer.cancel();
                    }
                }
                });
                lock.unlock();
            }
        };
    }

}
