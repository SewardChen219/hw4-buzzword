package controller;

import gui.Workspace;

import static controller.BuzzGameplayController.appTemplate;

/**
 * Created by SewardC on 2016/12/11.
 */
public class BuzzHelpController implements HelpController{
    @Override
    public void handleReturnRequest(){
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize(3);
    }
    public void handleLogoutRequest(){
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize(0);
    }

    public void handleExitRequest(){
        System.exit(0);
    }
}
