package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import propertymanager.PropertyManager;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.locks.ReentrantLock;

import static settings.AppPropertyType.ANIMAL_LIST;
import static settings.AppPropertyType.ANIME_LIST;
import static settings.InitializationParameters.APP_WORDDIR_PATH;

/**
 * Created by SewardC on 2016/11/14.
 */
public class BuzzLevelChoiceController implements LevelChoiceController {

    private AppTemplate appTemplate;
    private int        levelSelection;
    private ArrayList<String> arrayLists;
    private ArrayList<String> correctWordList;
    private ArrayList<Character> charList;

    private int l = 0;
    public void setAppTemplate(AppTemplate appTemplate){this.appTemplate = appTemplate;}

    public AppTemplate getAppTemplate(){
        return appTemplate;
    }

    @Override
    public void handleLevelSelectionRequest(){
    }

    @Override
    public void handleReturnRequest(){
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize(3);
    }

    public void handleLogoutRequest(){
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize(0);
    }

    public void handleExitRequest(){
        System.exit(0);
    }

    @FXML
    public void handleLevelEnterRequest(MouseEvent e) throws IOException{
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        GameData gamedata = (GameData) appTemplate.getDataComponent();
        int [] levelCompletions =  gamedata.getLevelCompletion();
        for(int i = 1; i<= 8; i++)
        if(e.getSource() == ((Label)gameWorkspace.getLevelChoiceScreenPane().lookup("#levelLabel" + i)) &&
                i <= levelCompletions[((BuzzModeChoiceController)appTemplate.getModeChioceController()).
                        getModeSelection()]+ 1) {
            levelSelection = i;

            layout(((BuzzModeChoiceController)appTemplate.getModeChioceController()).getModeSelection());
            gameWorkspace.reinitialize(5);
        }
    }

    public void layout(int modeSelection) throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        String list = "";
        if(modeSelection == 1) list = propertyManager.getPropertyValue(ANIME_LIST);
        else list = propertyManager.getPropertyValue(ANIMAL_LIST);
        FileInputStream fileInputStream = new FileInputStream(APP_WORDDIR_PATH.getParameter() + File.separator +
                list);
        DataInputStream inputStream = new DataInputStream(fileInputStream);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String strLine;

        arrayLists = new ArrayList<>();

        while((strLine = bufferedReader.readLine()) != null) {
            arrayLists.add(strLine);
        }
        inputStream.close();

        //String[] correctWords = new String[levelSelection/3+1];
        int totalScore = 0;
        /*
        for(int i=0; i < correctWords.length; i++) {
            int random = (int) (Math.random() * 50 + 1);
            if(arrayLists.get(random).length() >= 3) // words with less than 3 letter are excluded from score calculation
            {
                correctWords[i] = arrayLists.get(random);
                System.out.println(correctWords[i]);
                totalScore += (correctWords[i].length() / 2);
            }
            else
                i--;
        }



        do{
            charList = new ArrayList<>();
            for(int i=0; i < correctWords.length; i++){
                for(int j=0; j < correctWords[i].length(); j++){
                        charList.add(correctWords[i].charAt(j));
                }
            }
        }while(charList.size() > 16);
        */
        do {
            charList = new ArrayList<>();
            while (charList.size() < 16) {
                int random = (int) (Math.random() * 26);
                char characters = (char) ('a' + random);
                charList.add(characters);
            }

            correctWordList = new ArrayList();
            //System.out.println(charList.size());
            findCorrectWords(charList, new ArrayList<Character>(), new ArrayList<Integer>());
        }while(correctWordList.size() <= levelSelection /3);
        System.out.println("Finish searching");
        System.out.println(correctWordList.size());
        for(int i = 0; i < correctWordList.size(); i++)
            System.out.println(correctWordList.get(i));

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        ((Label) gameWorkspace.getGamePlayScreenPane().lookup("#levelLabel2")).setText("Level " + levelSelection);
        for(int i = 0; i < 16; i++){
            ((Label) gameWorkspace.getGamePlayScreenPane().lookup("#label" + (i+1))).setText(""+ charList.get(i));
        }
        ((VBox) gameWorkspace.getGamePlayScreenPane().lookup("#guessedWords")).getChildren().clear();
        ((VBox) gameWorkspace.getGamePlayScreenPane().lookup("#Scores")).getChildren().clear();
        for(int i = 0; i < correctWordList.size(); i++){
            ((VBox) gameWorkspace.getGamePlayScreenPane().lookup("#guessedWords")).getChildren().
                    add(new Label(correctWordList.get(i)));
            ((VBox) gameWorkspace.getGamePlayScreenPane().lookup("#Scores")).getChildren().add(new Label(
                    (correctWordList.get(i).length()/2) + ""));
        }

        ((Label) gameWorkspace.getGamePlayScreenPane().lookup("#totalScore")).setText("Total Points: " + totalScore);

        ((TextArea) gameWorkspace.getGamePlayScreenPane().lookup("#targetScore")).setText("Target: \n\n"
                + (int)(totalScore / 4 * 3 + 1) + " POINTS");

        gameWorkspace.setTime(1);
        int totalTime = 59;
        appTemplate.getGamePlayController().start();
        gameWorkspace.setTotalTime(totalTime);
}

    public void timeClock(){
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.resetTimer();
        gameWorkspace.getTimer().scheduleAtFixedRate(gameWorkspace.getTimeTask(), gameWorkspace.getTime(),
                gameWorkspace.getTime());
    }

    public void findCorrectWords(ArrayList<Character> charList, ArrayList<Character> currentList, ArrayList<Integer> numList){
        if(currentList.size() >= 3 && checkExist(currentList)){
            is_a_solution(currentList);
        }else {
            if(currentList.size() == 0) {
                for (int i = 0; i < charList.size(); i++) {
                    currentList.add(charList.get(i));
                    numList.add(i);
                    if ((i - 4) >= 0 && !numList.contains(i - 4)) {
                        currentList.add(charList.get(i - 4));
                        numList.add(i - 4);
                        findCorrectWords(charList, currentList, numList);
                    }
                    if ((i - 1) % 4 != 3 && (i - 1) >= 0 && !numList.contains(i - 1)) {
                        currentList.add(charList.get(i - 1));
                        numList.add(i - 1);
                        findCorrectWords(charList, currentList, numList);
                    }
                    if ((i + 4) < 16 && !numList.contains(i + 4)) {
                        currentList.add(charList.get(i + 4));
                        numList.add(i + 4);
                        findCorrectWords(charList, currentList, numList);
                    }
                    if ((i + 1) % 4 != 0 && (i + 1) < 16 && !numList.contains(i + 1)) {
                        currentList.add(charList.get(i + 1));
                        numList.add(i + 1);
                        findCorrectWords(charList, currentList, numList);
                    }
                }
            }
            else{
                int tempNum = numList.size()-1;
                if(numList.get(tempNum) >= 4 && !numList.contains(numList.get(tempNum) - 4)) {
                    currentList.add(charList.get(numList.get(tempNum) - 4));
                    numList.add(numList.get(tempNum) - 4);
                    findCorrectWords(charList, currentList, numList);
                    currentList.remove(currentList.size() - 1);
                    numList.remove(numList.size() - 1);
                }
                if((numList.get(tempNum)-1) % 4 != 3 && (numList.get(tempNum)-1) >= 0 && !numList.contains(numList.get(tempNum) - 1)) {
                    currentList.add(charList.get(numList.get(tempNum) - 1));
                    numList.add(numList.get(tempNum) - 1);
                    findCorrectWords(charList, currentList, numList);
                    currentList.remove(currentList.size() - 1);
                    numList.remove(numList.size() - 1);
                }
                if(numList.get(tempNum) < 12 && !numList.contains(numList.get(tempNum) + 4)) {
                    currentList.add(charList.get(numList.get(tempNum) + 4));
                    numList.add(numList.get(tempNum) + 4);
                    findCorrectWords(charList, currentList, numList);
                    currentList.remove(currentList.size() - 1);
                    numList.remove(numList.size() - 1);
                }
                if((numList.get(tempNum)+1) % 4 != 0 && (numList.get(tempNum)+1) < 16 && !numList.contains(numList.get(tempNum) + 1)) {
                    currentList.add(charList.get(numList.get(tempNum) + 1));
                    numList.add(numList.get(tempNum) + 1);
                    findCorrectWords(charList, currentList, numList);
                    currentList.remove(currentList.size() - 1);
                    numList.remove(numList.size() - 1);
                }
            }
        }
    }

    public ArrayList<Character> getCharList(){
        return charList;
    }

    public boolean checkExist(ArrayList<Character> currentList){
        String word = "";
        for(int i=0; i< currentList.size(); i++)
            word += currentList.get(i);
        for(int i=0; i< arrayLists.size(); i++)
            if(word.equals(arrayLists.get(i))){
                System.out.println("found");
                return true;}
        return false;
    }

    public void is_a_solution(ArrayList<Character> currentList){
        String word = "";
        for(int i=0; i< currentList.size(); i++)
            word += currentList.get(i);
        for(int i = 0; i < correctWordList.size(); i++)
            if(word.equals(correctWordList.get(i)))
                return;
        System.out.println("-----");
        correctWordList.add(word);
    }

    public int getLevelSelection(){
        return levelSelection;
    }
}
