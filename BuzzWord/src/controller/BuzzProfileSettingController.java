package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static settings.InitializationParameters.APP_TABLE_WORKDIR_PATH;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * Created by SewardC on 2016/11/14.
 */
public class BuzzProfileSettingController implements ProfileSettingController {

    private GameData gamedata;

    @FXML
    protected PasswordField name;

    @FXML
    protected PasswordField password;

    private AppTemplate appTemplate;

    public void setAppTemplate(AppTemplate appTemplate){
        this.appTemplate = appTemplate;
        gamedata = (GameData) this.appTemplate.getDataComponent();
    }
    public void handleLoginReturnRequest() throws IOException {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize(3);
    }

    public void handleExitRequest(){
        System.exit(0);
    }

    public void handleCompleteAction() throws IOException{
        if(!name.getText().isEmpty() || !password.getText().isEmpty()) {
            URL workDirURL = new File(BuzzProfileSettingController.class.getClassLoader().getResource("").getFile(),
                    APP_WORKDIR_PATH.getParameter()).toURL();
            File saveDir = new File(workDirURL.getFile());
            gamedata.setName(name.getText());
            gamedata.setPassWord(password.getText());
            gamedata.setFirstTimeRegistered();
            File tableFile = new File(saveDir.toString() + APP_TABLE_WORKDIR_PATH.getParameter());
            if (!saveDir.exists()) {
                saveDir.mkdirs();
                this.appTemplate.getFileComponent().initTable(tableFile.toPath());
                File newFile = new File(saveDir.toString() + File.separator + name.getText() + ".json");
                this.appTemplate.getFileComponent().saveData(gamedata, newFile.toPath());
            } else {
                if (this.appTemplate.getFileComponent().checkData(name.getText(), tableFile.toPath())) {
                    File newFile = new File(saveDir.toString() + File.separator + name.getText() + ".json");
                    this.appTemplate.getFileComponent().saveData(gamedata, newFile.toPath());

                }
            }
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize(3);
        }
    }
}
