package controller;

import apptemplate.AppTemplate;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import propertymanager.PropertyManager;
import settings.AppPropertyType;
import ui.YesNoCancelDialogSingleton;

import javafx.scene.input.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;

import static settings.AppPropertyType.EXIT_MESSAGE;

/**
 * Created by SewardC on 2016/11/14.
 */
public class BuzzGameplayController implements GamePlayController {
    public static AppTemplate appTemplate;

    private Workspace gameWorkspace;
    private ArrayList<Integer> selectedList;
    private ArrayList<Character> charList;

    public void setAppTemplate(AppTemplate appTemplate){this.appTemplate = appTemplate;
    selectedList = new ArrayList<>();
        System.out.println("gameWorkspace: " + gameWorkspace);
    }

    @Override
    public void playWithTyping(){
        AnimationTimer timer = new AnimationTimer(){
            @Override
            public void handle(long now) {
                ((Workspace)appTemplate.getWorkspaceComponent()).getGamePlayScreenPane().getScene().setOnKeyTyped((KeyEvent event) -> {
                    char chosenChar = event.getCharacter().charAt(0);
                    System.out.println(chosenChar);
                    if(chosenChar >= 'a' && chosenChar <= 'z'){
                        for(int i = 1; i <= 16; i++){
                            if(chosenChar == charList.get(i-1)) {
                                Circle node = (Circle) gameWorkspace.getGamePlayScreenPane().lookup("#circle" + i);
                                node.setEffect(new DropShadow(5, 0, 0, Color.RED));
                                Label label = (Label) gameWorkspace.getGamePlayScreenPane().lookup("#label" + i);
                                label.setFont(new Font("Courier New", 12));
                            }
                        }
                    }
                });
            }

            public void stop(){

            }
        };
        timer.start();
    }

    public void start(){
        gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        charList = ((BuzzLevelChoiceController)appTemplate.getLevelChoiceController()).getCharList();
        playWithTyping();
    }

    // Later Work
    @Override
    public void playWithMouseDrag(){}

    @Override
    public void handlePauseRequest(){
        gameWorkspace.reinitialize(6);
    }

    // Later Use
    @Override
    public void handleResumeRequest(){}

    // Later Use
    @Override
    public void handleReplayRequest(){}

    // Later Work
    @Override
    public void handleNextRequest(){}


    public void handleLogoutRequest(){
        gameWorkspace.getTimer().cancel();
        gameWorkspace.reinitialize(0);
    }


    public void handleReturnRequest(){
        gameWorkspace.getTimer().cancel();
        gameWorkspace.reinitialize(3);
    }


    public void handleExitRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        YesNoCancelDialogSingleton dialog = YesNoCancelDialogSingleton.getSingleton();
        dialog.show(propertyManager.getPropertyValue(EXIT_MESSAGE));
    }

    public void handleReplayEvent(){
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize(5);
    }




    @FXML
    public void handleHoveringEffectRequest(MouseEvent e){
        for(int i = 1; i <= 16; i++)
        if(e.getSource() == ((Circle)gameWorkspace.getGamePlayScreenPane().lookup("#circle" + i)) ||
                e.getSource() == (Label)gameWorkspace.getGamePlayScreenPane().lookup("#label"+ i)){
            Circle circle = (Circle)gameWorkspace.getGamePlayScreenPane().lookup("#circle" + i);
            circle.setEffect(new DropShadow(5, 0, 0, Color.AQUA));
            Label label = (Label)gameWorkspace.getGamePlayScreenPane().lookup("#label" + i);
            label.setFont(new Font("Courier New", 12));
        }
    }
    public void handleMouseClickRequest(MouseEvent e){
        for(int i = 1; i <= 16; i++)
            if(e.getSource() == ((Circle)gameWorkspace.getGamePlayScreenPane().lookup("#circle" + i)) ||
                    e.getSource() == (Label)gameWorkspace.getGamePlayScreenPane().lookup("#label"+ i)){
                Circle circle = (Circle)gameWorkspace.getGamePlayScreenPane().lookup("#circle" + i);
                circle.setEffect(new DropShadow(5, 0, 0, Color.RED));
                Label label = (Label)gameWorkspace.getGamePlayScreenPane().lookup("#label" + i);
                label.setFont(new Font("Courier New", 12));
            }
    }

    /*
    public void handleMouseDragRequest(MouseEvent e){
        System.out.println("Yes1");
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        for(int i = 1; i <= 2; i++) {
            System.out.println("Yes");
            System.out.println(i);
            if (e.getSource() == ((Label) gameWorkspace.getGamePlayScreenPane().lookup("#connection" + i + "" + (i + 1)))) {
                Label label = ((Label) gameWorkspace.getGamePlayScreenPane().lookup("#connection" + i + "" + (i + 1)));
                label.setStyle("-fx-background: #000000");
            }

        }        for(int i = 1; i <= 2; i++) {
            if (e.getSource() == ((Label) gameWorkspace.getGamePlayScreenPane().lookup("#connection" + i + "" + (i + 1)))) {
                Label label2 = (Label) gameWorkspace.getGamePlayScreenPane().lookup("#label" + i);
                System.out.println("label" + i);
                Circle circle2 = (Circle) gameWorkspace.getGamePlayScreenPane().lookup("#circle" + i);
                System.out.println("circle" + i);
                if (label2.isPressed() || circle2.isPressed())
                    System.out.println("Yes");

                Label label = ((Label) gameWorkspace.getGamePlayScreenPane().lookup("#connection" + i + "" + (i + 1)));
                label.setStyle("-fx-background: #000000");
            }
        }
    }

    public void handleMouseDragExitRequest(MouseEvent e){
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        for(int i = 1; i <= 16; i++)
            if(e.getSource() == ((Circle)gameWorkspace.getGamePlayScreenPane().lookup("#circle" + i)) ||
                    e.getSource() == (Label)gameWorkspace.getGamePlayScreenPane().lookup("#label"+ i)){
                Circle circle = (Circle)gameWorkspace.getGamePlayScreenPane().lookup("#circle" + i);
                circle.setEffect(new DropShadow(5, 0, 0, Color.RED));
                Label label = (Label)gameWorkspace.getGamePlayScreenPane().lookup("#label" + i);
                label.setFont(new Font("Courier New", 12));
            }
    }
    */

    public void handleDragEvent(MouseEvent e){
        Circle circle = (Circle) gameWorkspace.getGamePlayScreenPane().lookup("#circle1");
        int j = 0;
        int k = 0;
        for(int i = 1; i <= 16; i++) {
        boolean norepeat = true;
            if ((e.getX() >= (circle.getCenterX() + j * 4 * circle.getRadius())
                    && e.getY() >= (circle.getCenterY() + k * 4 * circle.getRadius()) &&
                    e.getX() <= (circle.getCenterX()+  2 * circle.getRadius() + j * 4 * circle.getRadius()) &&
                    e.getY() <= (circle.getCenterY()+ 2 * circle.getRadius() + k * 4 * circle.getRadius())) &&
                    (selectedList.size() == 0 || (selectedList.get(selectedList.size()-1) == (j + (k-1)*4))
                            || (selectedList.get(selectedList.size()-1) == ((j-1) + k*4)) ||
                            (selectedList.get(selectedList.size()-1) == (j + (k+1)*4)) ||
                            (selectedList.get(selectedList.size()-1) == ((j+1) + k*4) ))){
                for(int z = 0; z < selectedList.size(); z++)
                    if(selectedList.get(z) == i)
                        norepeat = false;
                if(norepeat) {
                    Circle node = (Circle) ((GridPane) (gameWorkspace.getGamePlayScreenPane().lookup("#canvas"))).
                            getChildren().get(j + (k * 4));
                    node.setEffect(new DropShadow(5, 0, 0, Color.RED));
                    Label label = (Label) gameWorkspace.getGamePlayScreenPane().lookup("#label" + i);
                    label.setFont(new Font("Courier New", 12));
                    selectedList.add(i - 1);
                }
            }
            j++;
            if(j % 4 == 0) {
                j = 0;
                k++;
            }
        }
        if(selectedList.size() != 0)
        System.out.println("Char: " + selectedList.get(selectedList.size()-1));
        System.out.println("Size: " + selectedList.size());
    }

    public void handleMouseReleasedRequest(){
        for(int i = 1; i <= 16; i++){
            Circle circle = (Circle)gameWorkspace.getGamePlayScreenPane().lookup("#circle" + i);
            circle.setEffect(new DropShadow(10, 5, 5, Color.BLACK));
            Label label = (Label)gameWorkspace.getGamePlayScreenPane().lookup("#label" + i);
            label.setFont(new Font("Courier New", 10));
        }
        selectedList.clear();

    }

    public void handleBackToNormalRequest(MouseEvent e){
        for(int i = 1; i <= 16; i++)
            if((e.getSource() == (Circle)gameWorkspace.getGamePlayScreenPane().lookup("#circle" + i)||
                    e.getSource() == (Label)gameWorkspace.getGamePlayScreenPane().lookup("#label"+ i)) &&
                    (!((Circle)gameWorkspace.getGamePlayScreenPane().lookup("#circle" + i)).isPressed() &&
                            !((Label)gameWorkspace.getGamePlayScreenPane().lookup("#label"+ i)).isPressed())){
                Circle circle = (Circle)gameWorkspace.getGamePlayScreenPane().lookup("#circle" + i);
                circle.setEffect(new DropShadow(10, 5, 5, Color.BLACK));
                Label label = (Label)gameWorkspace.getGamePlayScreenPane().lookup("#label" + i);
                label.setFont(new Font("Courier New", 10));
            }
    }

}
