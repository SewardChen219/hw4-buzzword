package controller;

import apptemplate.AppTemplate;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import components.AppComponentBuilder;
import data.GameData;
import gui.Workspace;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import propertymanager.PropertyManager;
import settings.InitializationParameters;
import ui.AppMessageDialogSingleton;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_TABLE_WORKDIR_PATH;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * Created by SewardC on 2016/11/14.
 */
public class BuzzFileController implements FileController {

    private PropertyManager propertyManager = PropertyManager.getManager();
    private static AppTemplate appTemplate;

    public static final String REGISTERED_NAME              = "REGISTERED_PROFILE_NAME";
    public static final String PASSWORD                     = "PASSWORD";

    private GameData gameData;

    @FXML
    private PasswordField name;

    @FXML
    private PasswordField password;

    @FXML
    private Button newButton;

    @FXML
    private Button loginButton;

    //private Image img = new Image(propertyManager.getPropertyValue(EXIT_ICON));

    @FXML
    private ImageView imgView;


    public BuzzFileController(){}

    public BuzzFileController(AppTemplate appTemplate){
        this.appTemplate = appTemplate;
        gameData = new GameData(appTemplate);
    }

    /*
    public Button getNewButton(){
        return newButton;
    }
    public Button getLoginButton(){
        return loginButton;
    }
    public ImageView getExitButton(){
        return imgView;
    }
*/

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialogSingleton = AppMessageDialogSingleton.getSingleton();
        messageDialogSingleton.changeCloseButtonText(InitializationParameters.SUCCESS_DIALOG_BUTTON_LABEL.getParameter());
            messageDialogSingleton.show(propertyManager.getPropertyValue(NEW_COMPLETED_MESSAGE));
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize(0);
    }

    public void setAppTemplate(AppTemplate appTemplate) {this.appTemplate = appTemplate;}

    public AppTemplate getAppTemplate(){return appTemplate;}

    @Override
    public void handleCreateNewProfileRequest(){

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize(1);
    }

    @Override
    public void handleLoginOrLogoutRequest() throws IOException {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize(2);
    }

    // Not needed to be implemented yet.
    @Override
    public void handleShowProfileSettingRequest(){}

    @Override
    public void handleSaveRequest() throws IOException{}

    @Override
    public void handleHelpRequest() throws IOException{}

    public void handleCompleteRequest() throws IOException{
        URL workDirURL = new File(BuzzProfileSettingController.class.getClassLoader().getResource("").getFile(),
                APP_WORKDIR_PATH.getParameter()).toURL();
        File saveDir = new File(workDirURL.getFile());
        File tableFile = new File(saveDir.toString() + APP_TABLE_WORKDIR_PATH.getParameter());
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        if(appTemplate.getFileComponent().checkData2(name.getText(), password.getText(), tableFile.toPath())){
            File newFile = new File(saveDir.toString() + File.separator + name.getText() + ".json");
            appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), newFile.toPath());
            gameWorkspace.reinitialize(3);
        }
        else{
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(COMPLETE_ERROR));
            gameWorkspace.reinitialize(0);
        }
    }

    public void handleHideRequest() {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize(0);
    }

    @Override
    public void handleExitRequest(){
        System.exit(0);
    }
}
