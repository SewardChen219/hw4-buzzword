package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import propertymanager.PropertyManager;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import static settings.AppPropertyType.HELP;
import static settings.AppPropertyType.HELP_FXML;
import static settings.InitializationParameters.APP_WORDDIR_PATH;

/**
 * Created by SewardC on 2016/11/14.
 */
public class BuzzModeChoiceController implements ModeChoiceController {

    @FXML
    BorderPane root;

    private AppTemplate appTemplate;
    private String mode;
    private int modeSelection = 0;

    public void setAppTemplate(AppTemplate appTemplate){this.appTemplate = appTemplate;}

    @Override
    public void handleModeChoiceRequest(){
    }


    public void handleLogoutRequest(){
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize(0);
    }

    public void handlePlayRequest(){
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        root = gameWorkspace.getModeChoiceScreenPane();
        if(((ComboBox)root.lookup("#modeCombo")).getSelectionModel().getSelectedItem() == null)
            mode = "Famous People";
        else
            mode = ((ComboBox)root.lookup("#modeCombo")).getSelectionModel().getSelectedItem().toString();

        GameData gamedata = (GameData) appTemplate.getDataComponent();

        int [] levelCompletions =  gamedata.getLevelCompletion();

        if(mode.equals("Animals")) modeSelection = 0;
        else if(mode.equals("Anime")) modeSelection = 1;
        else if(mode.equals("Famous People")) modeSelection = 2;
        else if(mode.equals("English Dictionary")) modeSelection = 3;
        else if(mode.equals("Places")) modeSelection = 4;
        else modeSelection = 5;

        int succeededLevels = levelCompletions[modeSelection];
        for(int i = 1; i <= succeededLevels+1; i++) {
            if (i != 9) {
                ((Circle) gameWorkspace.getLevelChoiceScreenPane().lookup("#circle" + i)).
                        setFill(Color.rgb(149, 157, 164));
                ((Label) gameWorkspace.getLevelChoiceScreenPane().lookup("#levelLabel" + i))
                        .setTextFill(Color.rgb(33, 32, 32));
            }
        }
        gameWorkspace.reinitialize(4);
    }

    public int getModeSelection(){
        return modeSelection;
    }

    public String getMode() {
        return mode;
    }

    public void handleHelpRequest() throws IOException {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        PropertyManager propertyManager = PropertyManager.getManager();
        Scanner scanner = new Scanner(new File(APP_WORDDIR_PATH.getParameter() + File.separator
                + propertyManager.getPropertyValue(HELP)));
        ((TextArea) gameWorkspace.getHelpPane().lookup("#helpTextArea")).setWrapText(true);
        ((TextArea) gameWorkspace.getHelpPane().lookup("#helpTextArea")).setEditable(false);
        while(scanner.hasNext())
        ((TextArea) gameWorkspace.getHelpPane().lookup("#helpTextArea")).appendText(scanner.nextLine()+"\n");

        gameWorkspace.reinitialize(7);
    }

    public void handleReturnRequest(){
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize(3);
    }

    public void handleEditRequest(){
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ((Label) gameWorkspace.getProfileSettingPane().lookup("#nameLabel")).setText(
                ((GameData)appTemplate.getDataComponent()).getName());
        ((Label) gameWorkspace.getProfileSettingPane().lookup("#passwordLabel")).setText(
                ((GameData) appTemplate.getDataComponent()).getPassWord());
        gameWorkspace.reinitialize(8);
    }

    public void handleExitRequest(){
        System.exit(0);
    }
}
