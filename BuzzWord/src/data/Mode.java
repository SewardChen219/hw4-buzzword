package data;

/**
 * Created by SewardC on 2016/11/13.
 */
public enum Mode {

    ANIMAL("Animal"),
    ANIME("Anime"),
    FAMOUS_PEOPLE("Famous People"),
    ENGLISH_DICTIONARY("English Dictionary"),
    PLACES("Places"),
    SCIENCE("Science");

    public String mode;

     Mode(String mode){
        this.mode = mode;
    }

    public String getMode(){return mode;}
}
