package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import components.AppFileComponent;
import controller.BuzzProfileSettingController;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;

import javax.json.*;
import javax.json.stream.JsonGenerator;
import java.io.*;
import java.net.URL;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import static settings.AppPropertyType.PROPERTIES_LOAD_ERROR_MESSAGE;
import static settings.InitializationParameters.APP_TABLE_WORKDIR_PATH;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * Created by SewardC on 2016/11/12.
 */
public class GameDataFile implements AppFileComponent {

    public static final String REGISTERED_NAME              = "REGISTERED_PROFILE_NAME";
    public static final String PASSWORD                     = "PASSWORD";
    public static final String LEVEL_COMPLETION             = "LEVEL_COMPLETION";
    public static final String HIGH_SCORES                  = "HIGH_SCORES";
    public static final String CURRENT_MODE                 = "CURRENT_MODE";


    @Override
    public void saveData(AppDataComponent data, Path to) throws IOException {
        GameData gamedata = (GameData) data;
        JsonObject dataObject = Json.createObjectBuilder()
                .add(REGISTERED_NAME, gamedata.getName())
                .add(PASSWORD, gamedata.getPassWord())
                .add(LEVEL_COMPLETION, arrayOfLevelCompletion(gamedata))
                .add(HIGH_SCORES, arrayOfHighScores(gamedata))
                .add(CURRENT_MODE, gamedata.getCurrentMode())
                .build();

        Map<String, Object> dataProperties = new HashMap<>(1);
        dataProperties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(dataProperties);

        // Input everything into a Json file
        StringWriter stringWriter = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(stringWriter);
        jsonWriter.writeObject(dataObject);
        jsonWriter.close();

        // Make the file
        OutputStream outputStream = new FileOutputStream(to.toString());
        JsonWriter fileWriter = Json.createWriter(outputStream);
        fileWriter.writeObject(dataObject);
        PrintWriter printWriter = new PrintWriter(to.toString());
        printWriter.write(stringWriter.toString());
        printWriter.close();

        URL workDirURL = new File(BuzzProfileSettingController.class.getClassLoader().getResource("").getFile(),
                APP_WORKDIR_PATH.getParameter()).toURL();
        File saveDir = new File(workDirURL.getFile());
        File tableFile = new File(saveDir.toString() + APP_TABLE_WORKDIR_PATH.getParameter());
        if(gamedata.getFirstTimeRegistered()) addNameToTableIfNeeded(gamedata, tableFile);
    }

    public void addNameToTableIfNeeded(GameData gamedata, File tableFile){
        try {
            InputStream inputStream = new FileInputStream(tableFile.toString());
            JsonReader  jsonReader = Json.createReader(inputStream);
            JsonObject jsonObject = jsonReader.readObject();
            jsonReader.close();
            inputStream.close();
            JsonArrayBuilder names = Json.createArrayBuilder();
            for(int i = 0; i < jsonObject.getJsonArray(REGISTERED_NAME).size(); i++){
                names.add(jsonObject.getJsonArray(REGISTERED_NAME).getString(i));
            }
            names.add(gamedata.getName());

            JsonArrayBuilder passwords = Json.createArrayBuilder();
            for(int i = 0; i < jsonObject.getJsonArray(PASSWORD).size(); i++){
                passwords.add(jsonObject.getJsonArray(PASSWORD).getString(i));
            }
            passwords.add(gamedata.getPassWord());


            JsonObject dataObject = Json.createObjectBuilder()
                    .add(REGISTERED_NAME, names).
                            add(PASSWORD, passwords).build();

            Map<String, Object> dataProperties = new HashMap<>(1);
            dataProperties.put(JsonGenerator.PRETTY_PRINTING, true);
            JsonWriterFactory writerFactory = Json.createWriterFactory(dataProperties);

            // Input everything into a Json file
            StringWriter stringWriter = new StringWriter();
            JsonWriter jsonWriter = writerFactory.createWriter(stringWriter);
            jsonWriter.writeObject(dataObject);
            jsonWriter.close();

            OutputStream outputStream = new FileOutputStream(tableFile.toString());
            JsonWriter fileWriter = Json.createWriter(outputStream);
            fileWriter.writeObject(dataObject);
            PrintWriter printWriter = new PrintWriter(tableFile.toString());
            printWriter.write(stringWriter.toString());
            printWriter.close();


        }catch(Exception e){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager propertyManager = PropertyManager.getManager();
            dialog.show(propertyManager.getPropertyValue(PROPERTIES_LOAD_ERROR_MESSAGE));
        }
    }

    public JsonArrayBuilder arrayOfLevelCompletion(GameData gamedata){
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for(int i = 0; i < gamedata.getLevelCompletion().length; i++)
            arrayBuilder.add(gamedata.getLevelCompletion()[i] + "");
        return arrayBuilder;
    }

    public JsonArrayBuilder arrayOfHighScores(GameData gamedata){
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for(int i = 0; i < gamedata.getHighScores().length; i++)
            arrayBuilder.add(gamedata.getHighScores()[i] + "");
        return arrayBuilder;
    }
    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException {
        GameData gamedata = (GameData) data;
        try {
            InputStream inputStream = new FileInputStream(from.toString());
            JsonReader  jsonReader = Json.createReader(inputStream);
            JsonObject jsonObject = jsonReader.readObject();
            jsonReader.close();
            inputStream.close();

            gamedata.setName(jsonObject.getString(REGISTERED_NAME));
            gamedata.setPassWord(jsonObject.getString(PASSWORD));
            for(int i = 0; i < jsonObject.getJsonArray(LEVEL_COMPLETION).size(); i++)
                gamedata.setLevelCompletion(i, (int)(jsonObject.getJsonArray(LEVEL_COMPLETION).getString(i).charAt(0))-48);
            int j = 0, k = 0;
            for(int i = 0; i < jsonObject.getJsonArray(HIGH_SCORES).size(); i++) {
                gamedata.setHighScores(k, j, (int)(jsonObject.getJsonArray(HIGH_SCORES).getString(i).charAt(0))-48);
                j++;
                if (j % GameData.MAX_NUM_LEVELS == 0) {
                    j = 0;
                    k++;
                }
            }
            gamedata.setCurrentMode(jsonObject.getString(CURRENT_MODE));

        }catch(Exception e){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager propertyManager = PropertyManager.getManager();
            dialog.show(propertyManager.getPropertyValue(PROPERTIES_LOAD_ERROR_MESSAGE));
        }
    }

    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException {

    }

    @Override
    public boolean checkData(String dataStr, Path filePath) throws IOException{
        try {
            InputStream inputStream = new FileInputStream(filePath.toString());
            JsonReader  jsonReader = Json.createReader(inputStream);
            JsonObject jsonObject = jsonReader.readObject();
            jsonReader.close();
            inputStream.close();
            for(int i = 0; i < jsonObject.getJsonArray(REGISTERED_NAME).size(); i++){
                if(dataStr.equals(jsonObject.getJsonArray(REGISTERED_NAME).getString(i)))
                    return false;
            }
            return true;

        }catch(Exception e){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager propertyManager = PropertyManager.getManager();
            dialog.show(propertyManager.getPropertyValue(PROPERTIES_LOAD_ERROR_MESSAGE));
            return false;
        }
    }

    @Override
    public boolean checkData2(String name, String password, Path tablePath) throws IOException{
        try {
            InputStream inputStream = new FileInputStream(tablePath.toString());
            JsonReader  jsonReader = Json.createReader(inputStream);
            JsonObject jsonObject = jsonReader.readObject();
            jsonReader.close();
            inputStream.close();
            for(int i = 0; i < jsonObject.getJsonArray(REGISTERED_NAME).size(); i++){
                if(name.equals(jsonObject.getJsonArray(REGISTERED_NAME).getString(i))) {
                    if (password.equals(jsonObject.getJsonArray(PASSWORD).getString(i))) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }

            return false;

        }catch(Exception e){
            return false;
        }
    }
    @Override
    public void initTable(Path filePath) throws  IOException{
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder arrayBuilder1 = Json.createArrayBuilder();
        JsonObject dataObject = Json.createObjectBuilder()
                .add(REGISTERED_NAME, arrayBuilder).
                        add(PASSWORD, arrayBuilder1).build();
        Map<String, Object> dataProperties = new HashMap<>(1);
        dataProperties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(dataProperties);

        // Input everything into a Json file
        StringWriter stringWriter = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(stringWriter);
        jsonWriter.writeObject(dataObject);
        jsonWriter.close();

        // Make the file
        OutputStream outputStream = new FileOutputStream(filePath.toString());
        JsonWriter fileWriter = Json.createWriter(outputStream);
        fileWriter.writeObject(dataObject);
        PrintWriter printWriter = new PrintWriter(filePath.toString());
        printWriter.write(stringWriter.toString());
        printWriter.close();
    }
}
