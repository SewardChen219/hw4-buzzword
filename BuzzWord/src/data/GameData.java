package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by SewardC on 2016/11/12.
 */
public class GameData implements AppDataComponent{

    protected AppTemplate   appTemplate;
    private String          name;
    private String          passWord;
    private int[]           levelCompletions;
    private int[]           highScores;
    private int             currentLevel;
    private String            currentMode;
    private boolean         isDifficult;
    private ArrayList<String>   targetWords;
    private Set<String>     correctWords;
    private boolean         illegalWord = true;
    private boolean         difficulty = false;
    private boolean         firstTimeRegistered = false;
    private boolean         win = false;
    public static final int MAX_NUM_LEVELS = 16;
    public static final int MAX_NUM_MODE = 6;

    public GameData(AppTemplate appTemplate){
        this.appTemplate = appTemplate;
        levelCompletions = new int[MAX_NUM_MODE];
        highScores = new int[MAX_NUM_LEVELS * MAX_NUM_MODE];
        currentMode = Mode.ANIMAL.getMode();
        /* Will be edited later

        while(illegalWord) {
            illegalWord = false;
            this.targetWords = setTargetWord();
            char[] targetWordChars = targetWords.toCharArray();
            for (int i = 0; i < targetWordChars.length; i++)
                if (targetWordChars[i] < 97 || targetWordChars[i] > (97 + 26))
                    illegalWord = true;
        }

         */
        checkDifficulty();
        this.correctWords = new HashSet<>();
        /*
        Needs to initialize the data
         */
    }

    // Will be implemented
    public void checkDifficulty(){

    }

    public void setName(String name){
        this.name = name;
    }

    public void setPassWord(String passWord){
        this.passWord = passWord;
    }

    public void setFirstTimeRegistered(){
        firstTimeRegistered = true;
    }

    public String getName(){
        return name;
    }

    public String getPassWord(){
        return passWord;
    }

    public boolean getFirstTimeRegistered(){
        return firstTimeRegistered;
    }

    public String getCurrentMode(){
        return currentMode;
    }

    public void setCurrentMode(String mode) {
        currentMode = mode;
    }

    public int[] getLevelCompletion(){
        return levelCompletions;
    }

    public int[] getHighScores(){ return highScores;}

    public void setLevelCompletion(int mode, int level){
        levelCompletions[mode] = level;
    }

    public void setHighScores(int mode, int level, int highestScore){
        highScores[mode * MAX_NUM_LEVELS + level] = highestScore;
    }

    public boolean getWinOrLose(){
        return win;
    }
    // Reset the data in the current state only
    @Override
    public void reset(){

    }
}
