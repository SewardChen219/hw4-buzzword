package buzzword;

import apptemplate.AppTemplate;
import components.AppComponentBuilder;
import components.AppDataComponent;
import components.AppFileComponent;
import components.AppWorkspaceComponent;
import data.GameData;
import data.GameDataFile;
import gui.Workspace;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by SewardC on 2016/11/12.
 */

public class BuzzWord extends AppTemplate {
/*
    public class BuzzWord extends Application implements Initializable {

    static Stage stage;
    static Stage loginScreen;
    @FXML
    BorderPane root;

    @FXML
    GridPane root2;

    @FXML
    Button newButton;

    @FXML
    Button loginButton;

    @FXML
    ImageView imgView;

    @FXML
    ComboBox<String> modeCombo = new ComboBox<>();
    static ComboBox<String> tempStore;

    @FXML
    Label modeLabel = new Label();
    static Label tempLabel;

    @FXML
    Label modeLevel;

    static String choice = "Famous People";
    @Override
    // Can't really have too many fxml files at the same time.
    public void initialize(URL fxmlLocation, ResourceBundle resource){
        assert newButton != null : "newButton variable has something wrong";
        assert loginButton != null : "loginButton variable has something wrong";
        assert imgView != null  : "imgView ImageView has something wrong";

    }

    public static void main(String [] args){launch(args);}

    public void start(Stage primaryStage) throws IOException {
        primaryStage.setTitle("BuzzWord");
        stage = primaryStage;
        root = FXMLLoader.load(
                new URL(BuzzWord.class.getResource("Homescreen.fxml").
                        toExternalForm()));
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public void setOnNewAction() throws IOException{
        root = FXMLLoader.load(
                new URL(BuzzWord.class.getResource("CreateNewProfile1.fxml").
                        toExternalForm()));
        stage.setScene(new Scene(root));
    }

    public void setOnLoginAction() throws IOException{
        loginScreen = new Stage();
        loginScreen.initStyle(StageStyle.UNDECORATED);

        loginScreen.initModality(Modality.WINDOW_MODAL);
        loginScreen.initOwner(stage);
        root2 = FXMLLoader.load(
                new URL(BuzzWord.class.getResource("LoginScreen.fxml").
                        toExternalForm()));
        loginScreen.setScene(new Scene(root2));
        loginScreen.showAndWait();
    }

    public void setOnReturnAction() throws IOException{
        root = FXMLLoader.load(
                new URL(BuzzWord.class.getResource("Homescreen.fxml").
                        toExternalForm()));
        stage.setScene(new Scene(root));
    }

    public void setOnExitAction() throws IOException{
        System.exit(0);
    }

    public void setOnHideAction() throws IOException{
        loginScreen.close();
    }

    public void setOnCompleteAction() throws IOException{
        loginScreen.close();
        root = FXMLLoader.load(
                new URL(BuzzWord.class.getResource("ModeChoiceScreen.fxml").
                        toExternalForm()));
        stage.setScene(new Scene(root));
    }

    public void setOnCompleteAction2() throws IOException{
        root = FXMLLoader.load(
                new URL(BuzzWord.class.getResource("ModeChoiceScreen.fxml").
                        toExternalForm()));
        tempStore = modeCombo;
        stage.setScene(new Scene(root));
    }

    public void setOnPlayAction() throws IOException{
        switch(modeCombo.getSelectionModel().getSelectedItem()){
            case "Famous People":
                root = FXMLLoader.load(
                        new URL(BuzzWord.class.getResource("LevelChoiceScreen.fxml").
                                toExternalForm()));
                choice = "Famous People";
                break;
            case "Animals":
                root = FXMLLoader.load(
                        new URL(BuzzWord.class.getResource("LevelChoiceScreen1.fxml").
                                toExternalForm()));
                choice = "Animals";
                break;
            case "Dictionary":
                root = FXMLLoader.load(
                        new URL(BuzzWord.class.getResource("LevelChoiceScreen2.fxml").
                                toExternalForm()));
                choice = "Dictionary";

        }

        stage.setScene(new Scene(root));
    }

    public void setOnLevelAction(Event e) throws IOException{
        root = FXMLLoader.load(
                new URL(BuzzWord.class.getResource("GamePlayScreen.fxml").
                        toExternalForm()));

        stage.setScene(new Scene(root));
    }

    public void setOnHomeAction() throws IOException{
        root = FXMLLoader.load(
                new URL(BuzzWord.class.getResource("ModeChoiceScreen.fxml").
                        toExternalForm()));
        stage.setScene(new Scene(root));
    }

*/
    public static void main(String [] args){launch(args);}

    public String getFileControllerClass(){return "BuzzFileController";}

    @Override
    public AppComponentBuilder makeAppBuilderHook(){
        return new AppComponentBuilder() {
            @Override
            public AppDataComponent buildDataComponent() throws Exception {
                return new GameData(BuzzWord.this);
            }

            @Override
            public AppFileComponent buildFileComponent() throws Exception {
                return new GameDataFile();
            }

            @Override
            public AppWorkspaceComponent buildWorkspaceComponent() throws Exception {
                return new Workspace(BuzzWord.this);
            }
        };
    }

}